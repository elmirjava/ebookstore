package com.example.ebookstore.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Enum {
    Active(1),DeActive(0);
    private int value;
}
