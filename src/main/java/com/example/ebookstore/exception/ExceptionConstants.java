package com.example.ebookstore.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class ExceptionConstants {
    public static final Integer INTERNAL_EXCEPTION=100;
    public static final Integer INVALID_REQUEST_DATA=101;
    public static final Integer BOOK_NOT_FOUND=102;
    public static final Integer CUSTOMER_NOT_FOUND=103;
    public static final Integer USER_NOT_FOUND =104 ;
}
