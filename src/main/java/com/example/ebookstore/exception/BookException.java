package com.example.ebookstore.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BookException extends RuntimeException{
    private Integer code;

    public BookException(Integer code,String message) {
        super(message);
        this.code=code;
    }
    public Integer code(){
        return code;
    }
}
