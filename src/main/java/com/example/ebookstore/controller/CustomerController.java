package com.example.ebookstore.controller;

import com.example.ebookstore.dto.request.ReqBook;
import com.example.ebookstore.dto.request.ReqCustomer;
import com.example.ebookstore.dto.response.RespBook;
import com.example.ebookstore.dto.response.RespCustomer;
import com.example.ebookstore.dto.response.Response;
import com.example.ebookstore.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService customerService;
    @GetMapping("GetCustomerList")
    public ModelAndView getCustomerList(){
        ModelAndView model=new ModelAndView("customer/customerList");
        try{
           Response<List<RespCustomer>>response=customerService.customerList();
           if(response.getStatus().getCode()==1){
               model.addObject("customer",response.getT());
           }
        }catch (Exception e){
            e.printStackTrace();
        }
        return model;
    }
    @GetMapping("/NewCustomer")
    public ModelAndView newCustomer() {
        ModelAndView model = new ModelAndView("customer/newCustomer");
        return model;
    }

    @PostMapping("/AddCustomer")
    public RedirectView addCustomer(@ModelAttribute("reqCustomer") ReqCustomer reqCustomer) {
        customerService.addCustomer(reqCustomer);
        return new RedirectView("/bookstore/customer/GetCustomerList");
    }
    @GetMapping("/GetCustomerById/{id}")
    public ModelAndView getCustomerById(@PathVariable("id") Long id) {
        ModelAndView model=new ModelAndView("customer/getCustomerById");
        try {
            Response<RespCustomer> customer = customerService.getCustomerById(id);
            model.addObject("customer", customer.getT());
        }catch (Exception e){
            e.printStackTrace();
        }
        return model;
    }

    @PostMapping(value = "/UpdateCustomer")
    public RedirectView updateCustomer(@ModelAttribute("reqBook") ReqCustomer reqCustomer) {
        customerService.updateCustomer(reqCustomer);
        return new RedirectView("/bookstore/customer/GetCustomerList");
    }
    @GetMapping("/DeleteCustomer/{id}")
    public RedirectView deleteCustomer(@PathVariable Long id){
        customerService.deleteCustomer(id);
        return new RedirectView("bookstore/customer/GetCustomerList");
    }

}
