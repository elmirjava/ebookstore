package com.example.ebookstore.controller;

import com.example.ebookstore.service.MyUserDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final MyUserDetailService myUserDetailService;
    @GetMapping({" ","/LoginView"})
    public ModelAndView loginView(){
        ModelAndView model=new ModelAndView("loginView");
        return model;
    }
    @PostMapping("/Login")
    public RedirectView login(@RequestParam String username, HttpServletRequest request){
        RedirectView redirectView=new RedirectView(request.getContextPath() + "/book/index");
        UserDetails userDetails=myUserDetailService.loadUserByUsername(username);
        return redirectView;
    }

}
