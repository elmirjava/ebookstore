package com.example.ebookstore.controller;

import com.example.ebookstore.dto.request.ReqBook;
import com.example.ebookstore.dto.response.RespBook;
import com.example.ebookstore.dto.response.Response;
import com.example.ebookstore.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import java.util.List;


@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;


    @GetMapping({" ", "/index"})
    public ModelAndView index() {
        ModelAndView model = new ModelAndView("index");
        return model;
    }

    @GetMapping("/GetBookList")
    public ModelAndView getBookList() {
        ModelAndView model = new ModelAndView("books/bookList");
        try {
            Response<List<RespBook>> listResponse = bookService.getBookList();
            if (listResponse.getStatus().getCode() == 1) {
                model.addObject("bookList", listResponse.getT());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;
    }

    @GetMapping("/NewBook")
    public ModelAndView newBook() {
        ModelAndView model = new ModelAndView("books/newBook");
        return model;
    }

    @PostMapping("/AddBook")
    public RedirectView addBook(@ModelAttribute("reqBook") ReqBook reqBook) {
        bookService.addBook(reqBook);
        return new RedirectView("/bookstore/book/GetBookList");
    }
    @GetMapping("/GetBookById/{id}")
    public ModelAndView getBookById(@PathVariable("id") Long id) {
        ModelAndView model=new ModelAndView("books/getBookById");
        try {
            Response<RespBook> book = bookService.getById(id);
            model.addObject("book", book.getT());
        }catch (Exception e){
            e.printStackTrace();
        }
        return model;
    }

    @PostMapping(value = "/UpdateBook")
    public RedirectView updateBook(@ModelAttribute("reqBook") ReqBook reqBook) {
        bookService.updateBook(reqBook);
        return new RedirectView("/bookstore/book/GetBookList");
    }

    @GetMapping("/DeleteBookById/{id}")
    public RedirectView deleteBook(@PathVariable("id") Long id){
        bookService.deleteBook(id);
        return new RedirectView("/bookstore/book/GetBookList");
    }
}
