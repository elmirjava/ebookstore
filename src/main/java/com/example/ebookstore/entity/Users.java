package com.example.ebookstore.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@DynamicInsert
@Table(name = "user")
@NoArgsConstructor
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false,length = 50,unique = true)
    private String username;
    @Column(nullable = false,length = 70)
    private String password;
    private String role;
    @ColumnDefault(value ="1")
    private Integer active;
    public Users(String username,String password){
        this.username=username;
        this.password=password;
    }

}
