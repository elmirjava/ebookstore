package com.example.ebookstore.repository;

import com.example.ebookstore.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Users,Long> {
    Users findUserByUsernameAndActive(String username, Integer active);

}
