package com.example.ebookstore.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RespStatus {
    private Integer code;
    private String message;
    public static final Integer SUCCESS_CODE=1;
    public static final String SUCCESS_MESSAGE="success";
    public static RespStatus getSuccessMessage(){
        return new RespStatus(SUCCESS_CODE,SUCCESS_MESSAGE);
    }
}
