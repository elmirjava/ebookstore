package com.example.ebookstore.dto.response;

import lombok.Data;

@Data
public class RespLogin {
    private String username;
    private String password;
}
