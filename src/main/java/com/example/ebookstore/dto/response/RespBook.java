package com.example.ebookstore.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RespBook {
    private Long id;
    private String author;
    private String title;
    private String barCode;
    private Date year;
    private Double price;
    private String category;
}
