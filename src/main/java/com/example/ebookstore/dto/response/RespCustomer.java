package com.example.ebookstore.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RespCustomer {
    private Long id;
    private String name;
    private String surname;
    private Date dob;
    private String address;
    private String seria;
}
