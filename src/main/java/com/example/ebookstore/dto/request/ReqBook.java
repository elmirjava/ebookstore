package com.example.ebookstore.dto.request;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class ReqBook {
    private Long id;
    private String author;
    private String title;
    private String barCode;
    private String year;
    private Double price;
    private String category;
}
