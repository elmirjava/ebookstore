package com.example.ebookstore.dto.request;

import lombok.Data;

import javax.annotation.security.DenyAll;
import java.util.Date;
@Data
public class ReqCustomer {
    private Long id;
    private String name;
    private String surname;
    private String dob;
    private String address;
    private String seria;
}
