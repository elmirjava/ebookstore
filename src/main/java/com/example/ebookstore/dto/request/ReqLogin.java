package com.example.ebookstore.dto.request;

import lombok.Data;

@Data
public class ReqLogin {
    private String username;
    private String password;

}
