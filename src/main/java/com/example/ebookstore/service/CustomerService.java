package com.example.ebookstore.service;

import com.example.ebookstore.dto.request.ReqCustomer;
import com.example.ebookstore.dto.response.RespCustomer;
import com.example.ebookstore.dto.response.Response;

import java.util.List;
public interface CustomerService {
    Response<List<RespCustomer>> customerList();
    Response<RespCustomer>getCustomerById(Long customerId);
    Response addCustomer(ReqCustomer reqCustomer);
    Response updateCustomer(ReqCustomer reqCustomer);
    Response deleteCustomer(Long customerID);

}
