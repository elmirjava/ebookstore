package com.example.ebookstore.service;

import com.example.ebookstore.dto.request.ReqBook;
import com.example.ebookstore.dto.response.RespBook;
import com.example.ebookstore.dto.response.Response;

import java.util.List;

public interface BookService {
    Response<List<RespBook>> getBookList();
    Response addBook(ReqBook reqBook);
    Response<RespBook>getById(Long booId);
    Response updateBook(ReqBook reqBook);
    Response deleteBook(Long bookId);

}
