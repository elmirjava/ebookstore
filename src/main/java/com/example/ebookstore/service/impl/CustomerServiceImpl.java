package com.example.ebookstore.service.impl;

import com.example.ebookstore.dto.request.ReqCustomer;
import com.example.ebookstore.dto.response.RespCustomer;
import com.example.ebookstore.dto.response.RespStatus;
import com.example.ebookstore.dto.response.Response;
import com.example.ebookstore.entity.Customer;
import com.example.ebookstore.enums.Enum;
import com.example.ebookstore.exception.BookException;
import com.example.ebookstore.exception.ExceptionConstants;
import com.example.ebookstore.repository.CustomerRepository;
import com.example.ebookstore.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    DateFormat dateFormat=new SimpleDateFormat("YYYY-MM-DD");

    @Override
    public Response<List<RespCustomer>> customerList() {
        Response<List<RespCustomer>> response = new Response<>();
        try {
            List<Customer> customers = customerRepository.findAllByActive(Enum.Active.getValue());
            if (customers.isEmpty()) {
                throw new BookException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer Not Found");
            }
            List<RespCustomer> customerList = customers.stream().map(this::convert).collect(Collectors.toList());
            if (customerList.isEmpty()) {
                throw new BookException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer Not Found");
            }
            response.setT(customerList);
            response.setStatus(RespStatus.getSuccessMessage());
        } catch (BookException bookException) {
            bookException.printStackTrace();
            response.setStatus(new RespStatus(bookException.code(), bookException.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response<RespCustomer> getCustomerById(Long customerId) {
        Response<RespCustomer> response = new Response<>();
        try {
            Long id = customerId;
            if (id == null) {
                throw new BookException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid Request Data");
            }
            Customer customer = customerRepository.findCustomerByIdAndActive(id, Enum.Active.getValue());
            if (customer == null) {
                throw new BookException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer not Found");
            }
            RespCustomer respCustomer = convert(customer);
            response.setT(respCustomer);
            response.setStatus(RespStatus.getSuccessMessage());
        } catch (BookException bookException) {
            bookException.printStackTrace();
            response.setStatus(new RespStatus(bookException.code(), bookException.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response addCustomer(ReqCustomer reqCustomer) {
        Response response = new Response();
        try {
            String name = reqCustomer.getName();
            if (name == null) {
                throw new BookException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer Not Found");
            }
            Customer customer = Customer.builder()
                    .name(reqCustomer.getName())
                    .surname(reqCustomer.getSurname())
                    .dob(dateFormat.parse(reqCustomer.getDob()))
                    .address(reqCustomer.getAddress())
                    .seria(reqCustomer.getSeria())
                    .build();
            customerRepository.save(customer);
            response.setStatus(RespStatus.getSuccessMessage());
        } catch (BookException b) {
            b.printStackTrace();
            response.setStatus(new RespStatus(b.code(), b.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response updateCustomer(ReqCustomer reqCustomer) {
        Response response = new Response();
        try {
            Long customerId = reqCustomer.getId();
            String name = reqCustomer.getName();
            if (customerId == null || name == null) {
                throw new BookException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid Request Data");
            }
            Customer customer = customerRepository.findCustomerByIdAndActive(customerId, Enum.Active.getValue());
            if (Objects.isNull(customer)) {
                throw new BookException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer not Found");
            }
            customer=Customer.builder()
                    .id(customer.getId())
                    .name(reqCustomer.getName())
                    .surname(reqCustomer.getSurname())
                    .dob(dateFormat.parse(reqCustomer.getDob()))
                    .address(reqCustomer.getAddress())
                    .seria(reqCustomer.getSeria())
                    .dataDate(customer.getDataDate())
                    .active(customer.getActive())
                    .build();
            customerRepository.save(customer);
            response.setStatus(RespStatus.getSuccessMessage());
        } catch (BookException b) {
            b.printStackTrace();
            response.setStatus(new RespStatus(b.code(), b.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response deleteCustomer(Long customerID) {
        Response response = new Response();
        try {
            if (customerID == null) {
                throw new BookException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid Request Data");
            }
            Customer customer = customerRepository.findCustomerByIdAndActive(customerID, Enum.Active.getValue());
            if (customer == null) {
                throw new BookException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer Not Found");
            }
            customer.setActive(Enum.DeActive.getValue());
            customerRepository.save(customer);
            response.setStatus(RespStatus.getSuccessMessage());
        } catch (BookException bookException) {
            bookException.printStackTrace();
            response.setStatus(new RespStatus(bookException.code(), bookException.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    public RespCustomer convert(Customer customer) {
        RespCustomer respCustomer = RespCustomer.builder()
                .id(customer.getId())
                .name(customer.getName())
                .surname(customer.getSurname())
                .dob(customer.getDob())
                .address(customer.getAddress())
                .seria(customer.getSeria())
                .build();
        return respCustomer;
    }
}
