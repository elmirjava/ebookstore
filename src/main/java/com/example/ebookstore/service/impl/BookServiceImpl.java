package com.example.ebookstore.service.impl;

import com.example.ebookstore.dto.request.ReqBook;
import com.example.ebookstore.dto.response.RespBook;
import com.example.ebookstore.dto.response.RespStatus;
import com.example.ebookstore.dto.response.Response;
import com.example.ebookstore.entity.Book;
import com.example.ebookstore.enums.Enum;
import com.example.ebookstore.exception.BookException;
import com.example.ebookstore.exception.ExceptionConstants;
import com.example.ebookstore.repository.BookRepository;
import com.example.ebookstore.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    DateFormat dateFormat=new SimpleDateFormat("YYYY-MM-DD");
    private final BookRepository bookRepository;
    @Override
    public Response<List<RespBook>> getBookList(){
        Response<List<RespBook>>response=new Response<>();
        try{
            List<Book>books=bookRepository.findAllByActive(Enum.Active.getValue());
            if(books.isEmpty()){
                throw new BookException(ExceptionConstants.BOOK_NOT_FOUND,"Book not found");
            }
           List<RespBook>bookList= books.stream().map(this::convert).collect(Collectors.toList());
            response.setT(bookList);
            response.setStatus(RespStatus.getSuccessMessage());
        }catch (BookException bookException){
            bookException.printStackTrace();
            response.setStatus(new RespStatus(bookException.code(),bookException.getMessage()));
        }catch (Exception e){
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION,"Internal Exception"));
        }
        return response;
    }

    @Override
    public Response addBook(ReqBook reqBook) {
        Response response=new Response();
        try{
            String author=reqBook.getAuthor();
            if(author==null){
                throw new BookException(ExceptionConstants.BOOK_NOT_FOUND,"Book Not Found");
            }
            Book book=Book.builder()
                    .author(author)
                    .title(reqBook.getTitle())
                    .barCode(reqBook.getBarCode())
                    .year(dateFormat.parse((reqBook.getYear())))
                    .price(reqBook.getPrice()/100)
                    .category(reqBook.getCategory())
                    .build();
            bookRepository.save(book);
            response.setStatus(RespStatus.getSuccessMessage());
        }catch (BookException b){
            b.printStackTrace();
            response.setStatus(new RespStatus(b.code(),b.getMessage()));
        }catch (Exception e){
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION,"Internal Exception"));
        }
        return response;
    }

    @Override
    public Response<RespBook> getById(Long bookId) {
        Response<RespBook>response=new Response<>();
        try{
            Long id=bookId;
            if(id==null){
                throw new BookException(ExceptionConstants.INVALID_REQUEST_DATA,"Invalid Request Data");
            }
            Book book=bookRepository.findBookByIdAndActive(id,Enum.Active.getValue());
            if(book==null){
                throw new BookException(ExceptionConstants.BOOK_NOT_FOUND,"Book not Found");
            }
            RespBook respBook=convert(book);
            response.setT(respBook);
            response.setStatus(RespStatus.getSuccessMessage());
        }catch (BookException bookException){
            bookException.printStackTrace();
            response.setStatus(new RespStatus(bookException.code(),bookException.getMessage()));
        }catch (Exception e){
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION,"Internal Exception"));
        }
        return response;
    }

    @Override
    public Response updateBook(ReqBook reqBook) {
        Response response=new Response();
        try{
            Long bookId=reqBook.getId();
            String authorName= reqBook.getAuthor();
            if(bookId==null||authorName==null){
                throw new BookException(ExceptionConstants.INVALID_REQUEST_DATA,"Invalid Request Data");
            }
            Book book=bookRepository.findBookByIdAndActive(bookId,Enum.Active.getValue());
            if(book==null){
                throw new BookException(ExceptionConstants.BOOK_NOT_FOUND,"Book Not Found");
            }
            book=Book.builder()
                    .id(book.getId())
                    .author(reqBook.getAuthor())
                    .title(reqBook.getTitle())
                    .barCode(reqBook.getBarCode())
                    .year(dateFormat.parse((reqBook.getYear())))
                    .price(reqBook.getPrice())
                    .category(reqBook.getCategory())
                    .dataDate(book.getDataDate())
                    .active(book.getActive())
                    .build();
            bookRepository.save(book);
            response.setStatus(RespStatus.getSuccessMessage());
        }catch (BookException bookException){
            bookException.printStackTrace();
            response.setStatus(new RespStatus(bookException.code(),bookException.getMessage()));
        }catch (Exception e){
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION,"Internal Exception"));
        }
        return response;
    }

    @Override
    public Response deleteBook(Long id){
        Response response=new Response();
        try{
            Long bookId=id;
            if(id==null){
                throw new BookException(ExceptionConstants.INVALID_REQUEST_DATA,"Invalid request data");
            }
            Book book=bookRepository.findBookByIdAndActive(bookId,Enum.Active.getValue());
            if(book==null){
                throw new BookException(ExceptionConstants.BOOK_NOT_FOUND,"Book not found");
            }
            book.setActive(Enum.DeActive.getValue());
            bookRepository.save(book);
            response.setStatus(RespStatus.getSuccessMessage());
        }catch (BookException bookException){
            bookException.printStackTrace();
            response.setStatus(new RespStatus(bookException.code(),bookException.getMessage()));
        }catch (Exception e){
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION,"Internal Exception"));
        }
        return response;
    }


    public RespBook convert(Book book){
        RespBook respBook= RespBook.builder()
                .id(book.getId())
                .author(book.getAuthor())
                .title(book.getTitle())
                .barCode(book.getBarCode())
                .price(book.getPrice())
                .year(book.getYear())
                .category(book.getCategory())
                .build();
        return respBook;
    }
}
