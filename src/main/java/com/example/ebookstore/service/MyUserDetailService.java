package com.example.ebookstore.service;

import com.example.ebookstore.entity.MyUserDetails;
import com.example.ebookstore.entity.Users;
import com.example.ebookstore.enums.Enum;
import com.example.ebookstore.exception.BookException;
import com.example.ebookstore.exception.ExceptionConstants;
import com.example.ebookstore.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor

public class MyUserDetailService implements UserDetailsService {
    private final UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user=userRepository.findUserByUsernameAndActive(username, Enum.Active.getValue());
        if(user==null){
            throw new BookException(ExceptionConstants.USER_NOT_FOUND,"User not found");
        }
        MyUserDetails myUserDetails=new MyUserDetails(user);
        return myUserDetails;
    }
}
