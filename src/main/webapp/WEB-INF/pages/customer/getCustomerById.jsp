<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 10/26/2023
  Time: 4:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update Customer</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<form action="${pageContext.request.contextPath}/customer/UpdateCustomer" method="POST"modelattribute="reqCustomer">
    <input type="hidden" name="id" value="${customer.id}">
    <label>Name</label><input type="text" name="name" value="${customer.name}"/><br>
    <label>Surname</label><input type="text" name="surname" value="${customer.surname}"><br>
    <label>Date of birth</label><input type="text" name="dob" value="${customer.dob}"><br>
    <label>Address</label><input type="text" name="address" value="${customer.address}"><br>
    <label>Seria</label><input type="text" name="seria" value="${customer.seria}"><br>
    <button type="submit">Save</button>
</form>
</body>
</html>
