<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 10/24/2023
  Time: 3:50 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Customer List</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <h1>Customer List</h1>
            <a href="${pageContext.request.contextPath}/customer/NewCustomer"><input type="button" value="New Customer"></a>
            <table border="1" style="width: 100%" class="table table-dark">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Date of Birth</th>
                    <th>Address</th>
                    <th>Seria</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${customer}" var="c">
                    <tr>
                        <td>${c.id}</td>
                        <td>${c.name}</td>
                        <td>${c.surname}</td>
                        <td>${c.dob}</td>
                        <td>${c.address}</td>
                        <td>${c.seria}</td>
                        <td><a href="${pageContext.request.contextPath}/customer/GetCustomerById/${c.id}"><input
                                type="button" style="width: 100%" value="Edit"></a></td>
                        <td><a href="${pageContext.request.contextPath}/customer/DeleteCustomer/${c.id}"><input
                                type="button" style="width: 100%" value="Delete"></a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>
