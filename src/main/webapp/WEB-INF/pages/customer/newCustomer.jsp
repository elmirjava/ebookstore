<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 10/26/2023
  Time: 4:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>New Customer</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<h1>New Customer</h1>
<form action="${pageContext.request.contextPath}/customer/AddCustomer" method="POST" modelattribute="reqCustomer">
    <label>Name</label><input type="text" name="name" placeholder="Name"/><br>
    <label>Surname</label><input type="text" name="surname" placeholder="Surname"><br>
    <label>Date of birth</label><input type="text" name="dob" placeholder="YYYY-MM-DD"><br>
    <label>Address</label><input type="text" name="address" placeholder="Address"><br>
    <label>Seria</label><input type="text" name="seria" placeholder="Seria"><br>
    <input type="submit" value="Save">
</form>
</body>
</html>
