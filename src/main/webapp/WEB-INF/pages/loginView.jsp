<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 11/2/2023
  Time: 1:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login View</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<div style="text-align: center" >
<form action="${pageContext.request.contextPath}/user/Login" method="POST">
    <table border="1" style="width: 100%"><br/>
        <label>Username</label><input type="text" name="username" placeholder="Username"><br/><br/>
        <label>Password</label><input type="password" name="password" placeholder="Password"><br/><br/>
        <input type="submit" name="Log In">
    </table>
</form>
</div>
</body>
</html>
