<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 10/20/2023
  Time: 3:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Virtual Book Store</title>
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <a href="${pageContext.request.contextPath}/book/GetBookList"><input type="button" value="Book List"></a>
        <a href="${pageContext.request.contextPath}/customer/GetCustomerList"><input type="button"
                                                                                     value="Customer List"></a>
        <a href="${pageContext.request.contextPath}/register/Registration"><input type="button"
                                                                                  value="Registration"></a>
        <a href="${pageContext.request.contextPath}/user/Login"><input type="button" value="Login"></a>

    </div>
</div>
<img src="https://miro.medium.com/v2/resize:fit:1400/1*NZTwTuUhi_3Ah6-ZDROkNg.jpeg" alt="EBookStore" height="100%" width="100%" border="1">
</body>
</html>
