<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 10/21/2023
  Time: 12:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Book List</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<div class="container" style="text-align: center">
    <div class="row">
        <h1>Book List</h1>
        <a href="${pageContext.request.contextPath}/book/NewBook"><input type="button" value="New Book"></a>
        <table class="table" border="1" style="width: 100%">
            <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Author</th>
                <th>Title</th>
                <th>BarCode</th>
                <th>Year</th>
                <th>Price</th>
                <th>Category</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${bookList}" var="bl">
                <tr>
                    <td>${bl.id}</td>
                    <td>${bl.author}</td>
                    <td>${bl.title}</td>
                    <td>${bl.barCode}</td>
                    <td>${bl.year}</td>
                    <td>${bl.price}</td>
                    <td>${bl.category}</td>
                    <td><a href="${pageContext.request.contextPath}/book/GetBookById/${bl.id}"><input type="button"
                                                                                                      style="width: 100%"
                                                                                                      value="Edit"></a>
                    </td>
                    <td><a href="${pageContext.request.contextPath}/book/DeleteBookById/${bl.id}"><input type="button"
                                                                                                         style="width: 100%"
                                                                                                         value="Delete"></a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
