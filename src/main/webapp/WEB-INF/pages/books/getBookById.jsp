<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 10/21/2023
  Time: 1:54 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update Book</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<form action="${pageContext.request.contextPath}/book/UpdateBook" method="post" modelattribute="reqBook">
    <input type="hidden" name="id" value="${book.id}">
    <label>Author</label><input type="text" name="author" value="${book.author}"/><br>
    <label>Title</label><input type="text" name="title" value="${book.title}"><br>
    <label>BarCode</label><input type="text" name="barCode" value="${book.barCode}"><br>
    <label>Year</label><input type="text" name="year" value="${book.year}"><br>
    <label>Price</label><input type="text" name="price" value="${book.price}"><br>
    <label>Category</label><input type="text" name="category" value="${book.category}"><br>
    <button type="submit">Save</button>
</form>
</body>
</html>
