<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 10/20/2023
  Time: 9:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>New Book</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<h1>New Book</h1>
<form  action="${pageContext.request.contextPath}/book/AddBook" method="POST" modelAttribute="reqBook">
    <label>Author</label><input type="text" name="author" placeholder="Author name"/><br>
    <label>Title</label><input type="text" name="title" placeholder="Title"><br>
    <label>BarCode</label><input type="text" name="barCode" placeholder="BarCode"><br>
    <label>Year</label><input type="text" name="year" placeholder="YYYY-MM-DD"><br>
    <label>Price</label><input type="text" name="price" placeholder="Price"><br>
    <label>Category</label><input type="text" name="category" placeholder="Category"><br>
    <input type="submit" value="Save">
</form>
</body>
</html>
